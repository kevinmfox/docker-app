DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `phone` varchar(100) default NULL,
  `email` varchar(255) default NULL,
  `country` varchar(100) default NULL,
  `region` varchar(50) default NULL,
  `numberrange` mediumint default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Morgan Oliver","(600) 562-5520","quisque@hotmail.ca","South Korea","Mpumalanga",4),
  ("Melinda Gibbs","1-329-388-5017","ut.lacus@icloud.com","South Africa","Veneto",0),
  ("Gregory Ray","(662) 997-6281","lobortis.quis@icloud.org","Pakistan","Leinster",1),
  ("Janna Albert","(820) 972-7304","duis.volutpat@protonmail.ca","Philippines","Michoacán",9),
  ("Minerva Moody","1-529-445-6269","ipsum.phasellus@aol.org","Costa Rica","Tver Oblast",9),
  ("Kirsten Dalton","(927) 711-4294","ut@outlook.edu","Turkey","Västra Götalands län",5),
  ("Sophia Snow","1-233-328-6265","odio@google.com","Philippines","Henegouwen",2),
  ("Yolanda Baker","1-414-546-1651","ut.dolor@protonmail.ca","Turkey","Manisa",1),
  ("Zachary Edwards","(186) 574-2676","ut@google.net","Austria","Overijssel",3),
  ("Dexter Shelton","(162) 596-2944","luctus.curabitur@hotmail.ca","Brazil","Alberta",4);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Chloe Conner","1-570-254-2734","lorem@google.net","India","Baden Württemberg",6),
  ("Rylee Mccoy","1-579-629-4229","cursus@yahoo.org","Singapore","Carinthia",3),
  ("Pearl Owen","1-769-872-1233","non.magna@protonmail.couk","Spain","Manitoba",3),
  ("Christian Patel","1-735-858-3271","sed.est@outlook.couk","Pakistan","Cordillera Administrative Region",9),
  ("Ingrid Russell","1-542-657-6137","vulputate.ullamcorper.magna@hotmail.org","Vietnam","Eastern Visayas",5),
  ("Hector Rodriguez","(253) 368-6467","cras@outlook.org","Peru","Nevada",6),
  ("Shana Stevens","1-838-309-2001","mollis.integer@aol.org","Canada","West Bengal",2),
  ("Tatiana Reyes","(342) 405-8280","nunc.sed@aol.couk","Colombia","Limpopo",3),
  ("Tobias Roman","(868) 639-3448","nec.eleifend@aol.com","New Zealand","Magadan Oblast",7),
  ("Bradley Jackson","(444) 536-2647","magna.a@outlook.net","Italy","Oaxaca",8);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Caesar Mcfadden","1-464-819-8526","augue@protonmail.edu","Turkey","Tasmania",9),
  ("Madeline Bean","1-354-427-6841","a.scelerisque@icloud.ca","Austria","East Region",5),
  ("Ahmed Shepherd","(296) 863-4661","sit.amet@protonmail.ca","Spain","Piura",2),
  ("Keiko Rhodes","(380) 946-8356","mollis.dui@yahoo.ca","Turkey","Wiltshire",4),
  ("Allen Weiss","1-320-870-4411","hendrerit.id.ante@outlook.edu","Australia","Małopolskie",10),
  ("Kenneth Talley","1-581-728-5940","vitae.diam.proin@icloud.ca","Austria","Davao Region",8),
  ("Zephr Greer","(426) 731-5065","taciti.sociosqu.ad@google.net","Costa Rica","Santa Catarina",2),
  ("Kelly Schneider","(726) 298-3465","at.pretium@aol.couk","Australia","Australian Capital Territory",10),
  ("Winter Horton","1-574-344-9339","accumsan.neque.et@outlook.com","Indonesia","Querétaro",5),
  ("Florence Wood","(274) 351-1852","vehicula@outlook.couk","Ireland","Saratov Oblast",0);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Derek Ferrell","1-885-801-8466","adipiscing@google.com","Sweden","Punjab",5),
  ("Amena Schultz","(351) 253-3153","fames.ac.turpis@google.com","Peru","Ross-shire",6),
  ("Charissa Kim","(816) 777-1602","facilisi.sed@protonmail.couk","China","FATA",4),
  ("Leandra Odom","1-265-243-6402","molestie.pharetra@icloud.couk","Nigeria","New Brunswick",8),
  ("Raja Shaffer","1-657-677-3349","gravida.non@aol.com","New Zealand","Rogaland",1),
  ("Ignacia Moore","(665) 283-9316","pede@icloud.org","Colombia","Arkansas",6),
  ("Elliott Foley","1-874-167-1223","augue.malesuada@yahoo.ca","Austria","Comunitat Valenciana",6),
  ("Cullen Richmond","1-437-217-3638","auctor.nunc@aol.ca","Ireland","Central Java",2),
  ("Rahim Berger","1-272-705-3951","mauris.molestie@aol.net","Vietnam","Gauteng",9),
  ("Deirdre Holmes","1-891-400-7056","dolor.tempus@hotmail.net","Australia","Limpopo",7);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Gannon Arnold","1-826-656-9935","consequat@aol.net","Ireland","Bangsamoro",7),
  ("Igor Reilly","(574) 371-6232","phasellus.nulla@icloud.com","Germany","Swiętokrzyskie",7),
  ("Debra Robertson","1-153-676-9740","orci@outlook.com","Turkey","Ceuta",7),
  ("Abigail Mills","1-458-869-4297","euismod.est@icloud.edu","Philippines","Northern Territory",1),
  ("Tanner Daniels","1-553-771-9047","eu.eleifend@google.ca","Sweden","An Giang",5),
  ("Paula Hurley","(147) 784-5532","praesent.eu@google.com","Norway","Metropolitana de Santiago",5),
  ("Joel Gilbert","(198) 612-8446","at.auctor@hotmail.edu","Turkey","Friesland",8),
  ("Rahim Hampton","1-653-352-4823","vitae@google.couk","Netherlands","Île-de-France",3),
  ("Laith Cannon","(780) 815-3983","volutpat.nulla@google.couk","Brazil","Catalunya",5),
  ("Lawrence Puckett","1-708-230-6440","vivamus.rhoncus@protonmail.couk","Pakistan","Delta",4);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Jermaine Austin","1-510-388-3907","sapien.gravida.non@aol.com","Brazil","Liguria",3),
  ("Akeem Graves","1-433-904-8515","dolor.nonummy.ac@yahoo.edu","India","Bremen",0),
  ("Azalia Curry","(282) 278-4947","auctor@protonmail.edu","Ireland","Sonora",1),
  ("Kirby Larson","(814) 782-0351","cursus.nunc@aol.edu","France","Quindío",3),
  ("Mason Wilder","1-728-740-6744","dictum.magna@outlook.couk","Brazil","North Jeolla",9),
  ("Noble Clemons","(196) 468-6035","odio.sagittis@icloud.org","Ukraine","Bali",8),
  ("Myles Fry","(105) 630-4434","commodo.hendrerit.donec@aol.net","Canada","West-Vlaanderen",8),
  ("Hoyt Sweet","1-456-256-4801","tincidunt@icloud.org","Vietnam","Jeju",4),
  ("Wendy Chandler","1-187-641-3174","dictum@icloud.com","Belgium","Limburg",9),
  ("Tate Parsons","(113) 296-7564","neque@hotmail.com","India","North Maluku",3);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Stewart Serrano","(274) 372-7182","donec.felis@yahoo.org","Mexico","Western Australia",7),
  ("Brenda Wilkinson","1-221-216-7314","et@aol.org","Netherlands","FATA",8),
  ("Germaine Mccormick","(990) 317-9488","lorem.ac@hotmail.net","Poland","Tripura",9),
  ("Riley Luna","1-682-436-6826","lectus.ante.dictum@google.ca","Indonesia","Cao Bằng",0),
  ("William Christian","(359) 922-7316","eu@aol.org","Norway","Vaupés",9),
  ("Arsenio Sargent","1-668-943-3779","euismod.ac.fermentum@outlook.couk","Russian Federation","Cusco",7),
  ("Jemima Rivers","(437) 413-1524","faucibus.orci@icloud.org","Ireland","Östergötlands län",5),
  ("Jamal Macias","1-289-760-2575","commodo.auctor@aol.edu","Turkey","Texas",2),
  ("Bianca Hewitt","1-334-823-0589","dui.semper@protonmail.org","Turkey","Oyo",7),
  ("Uriel Wiley","(516) 312-3278","interdum.ligula.eu@google.com","Chile","Anambra",1);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Belle Holmes","(266) 642-3961","gravida.molestie.arcu@hotmail.ca","Ukraine","Van",7),
  ("Meredith Avery","(741) 742-1264","ridiculus.mus@hotmail.com","South Africa","Poltava oblast",8),
  ("Christen Caldwell","1-285-846-3133","sem.vitae@aol.org","Russian Federation","Somerset",0),
  ("Kellie Berger","1-478-286-9522","commodo.tincidunt@aol.ca","Philippines","Mecklenburg-Vorpommern",7),
  ("Astra Sawyer","(840) 276-9261","dictum.proin@yahoo.net","Peru","Zeeland",10),
  ("Jessamine Weiss","(562) 686-7960","nulla.integer@aol.com","India","Jeju",3),
  ("Eve Chaney","(819) 238-5075","dapibus.id@aol.org","Costa Rica","Lower Austria",2),
  ("Tarik Wolf","(265) 717-6351","velit@yahoo.couk","Costa Rica","İzmir",6),
  ("Rama Joyce","1-276-215-8114","maecenas.libero@yahoo.org","India","Ivano-Frankivsk oblast",5),
  ("Gavin Harrington","1-252-762-9690","quisque.purus@outlook.net","Spain","Aberdeenshire",8);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Cooper Baxter","1-537-478-4333","consectetuer.adipiscing.elit@google.ca","Mexico","South Sumatra",8),
  ("Penelope Malone","(678) 151-7258","nec.diam.duis@yahoo.edu","Germany","Noord Brabant",4),
  ("Blaze Jordan","(591) 436-8761","mus.donec@protonmail.org","Sweden","Madhya Pradesh",2),
  ("Kirk Keith","1-641-149-9242","netus.et@outlook.edu","France","Friesland",3),
  ("Baxter Welch","(708) 506-7868","nunc.ullamcorper@protonmail.org","Russian Federation","İzmir",3),
  ("Elton Vargas","(279) 178-3281","egestas.ligula@google.com","Mexico","FATA",1),
  ("Doris Clements","(228) 971-8612","nullam@hotmail.edu","Pakistan","Donetsk oblast",5),
  ("Olga Reeves","(471) 362-4882","consectetuer.euismod.est@hotmail.ca","New Zealand","Noord Holland",8),
  ("Yolanda Craft","(256) 683-9175","ipsum.phasellus@hotmail.ca","Canada","Flintshire",2),
  ("Germaine Gould","(881) 603-9335","morbi@yahoo.net","Peru","Tyrol",3);
INSERT INTO `people` (`name`,`phone`,`email`,`country`,`region`,`numberrange`)
VALUES
  ("Simon Blackburn","1-726-369-8092","nunc.mauris.elit@google.ca","China","Oryol Oblast",1),
  ("Forrest Gibbs","(590) 212-0779","felis@google.edu","Costa Rica","Valparaíso",5),
  ("Chaney Singleton","1-807-169-1320","enim.suspendisse@aol.com","Norway","Extremadura",9),
  ("Sara Wilkerson","(864) 661-4211","sem.consequat@outlook.org","Belgium","Utrecht",9),
  ("Amelia Crane","1-331-142-0231","id.enim@google.edu","South Africa","Gyeonggi",5),
  ("Deacon Bass","(528) 825-3720","non.arcu@icloud.couk","United Kingdom","Benue",2),
  ("Colin Curry","(186) 746-2578","posuere.vulputate@icloud.net","Singapore","Guanacaste",8),
  ("Bruno Hewitt","1-335-323-8445","consequat@protonmail.edu","Colombia","Jönköpings län",1),
  ("Ruby Fitzgerald","(755) 586-2335","integer.tincidunt@aol.ca","Russian Federation","Balıkesir",5),
  ("Kimberley Byrd","1-263-854-8785","tellus@google.edu","Ukraine","Munster",8);
