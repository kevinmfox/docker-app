<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    function getEnvVariableOrDefault($envVariable, $default)
    {
        $value = getenv($envVariable);
        if (!$value) { $value = $default; }
        return $value;
    }

    $apiEndpoint = getEnvVariableOrDefault("API_ENDPOINT", "192.168.16.130:8080");
?>