<?php
	session_start();
	include('common.php');

	$appHostname = '';
?>

<!DOCTYPE html>
<html class="has-navbar-fixed-top">

<head>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=2">
   	<title>Docker Lab</title>
   	<?php include('headCommon.php'); ?>
</head>
<body>

<?php include('navigation.php'); ?>

<section class="section">
    <div class="container">
		<?php include('commonTable.html'); ?>	
	</div>
</section>


</body>
</html>