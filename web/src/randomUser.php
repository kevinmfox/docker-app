<?php
	session_start();
	include('common.php');

	#make the api call
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, ("http://" . $apiEndpoint . "/api/randomUser"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	if (!$response) { print('Error calling API endpoint.'); }
	curl_close($ch);
	$data = json_decode($response, true);
	$appHostname = $data['appHostname'];
?>

<!DOCTYPE html>
<html class="has-navbar-fixed-top">

<head>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=2">
   	<title>Docker Lab</title>
   	<?php include('headCommon.php'); ?>
</head>
<body>

<?php include('navigation.php'); ?>

<section class="section">
    <div class="container">

		<?php include('commonTable.html'); ?>	

		<p class="title is-3"><?php echo $data['name']; ?></p>
        <p class="subtitle is-4"><?php echo $data['email']; ?></p>
		<p class="subtitle is-4"><?php echo $data['phone']; ?></p>
		<p class="subtitle is-4"><?php echo $data['country']; ?></p>
		<p class="subtitle is-4"><?php echo $data['region']; ?></p>
		<p class="subtitle is-4"><?php echo $data['numberrange']; ?></p>
        <br>
	</div>
</section>

</body>
</html>