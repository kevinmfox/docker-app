<?php
?>

<nav class="navbar is-fixed-top is-link" role="navigation" aria-label="main navigation">

  <div class="navbar-menu">
    <div class="navbar-start">
      <a href="/" class="navbar-item">
        Home
      </a>
      <a href="/randomUser" class="navbar-item">
        Random User
      </a>
      <a href="/allUsers" class="navbar-item">
        All Users
      </a>      
    </div>
  </div>
</nav>