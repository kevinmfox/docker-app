<?php
	session_start();
	include('common.php');

	#make the api call
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, ("http://" . $apiEndpoint . "/api/allUsers"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	if (!$response) { print('Error calling API endpoint.'); }	
	curl_close($ch);
	$data = json_decode($response, true);
	$appHostname = $data[0]['appHostname'];
?>

<!DOCTYPE html>
<html class="has-navbar-fixed-top">

<head>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=2">
   	<title>Docker Lab</title>
   	<?php include('headCommon.php'); ?>
</head>
<body>

<?php include('navigation.php'); ?>

<section class="section">
    <div class="container">
	
	<?php include('commonTable.html'); ?>	

    <table class="table is-striped is-bordered is-narrow is-hoverable is-fullwidth nowrap">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Country</th>
				<th>Region</th>
				<th>Number</th>
			</tr>
		</thead>
		<tbody>    
			<?php
				foreach ($data as $person)
				{
					echo "<tr>";
					echo "<td>".$person['name']."</td>";
					echo "<td>".$person['email']."</td>";
					echo "<td>".$person['phone']."</td>";
					echo "<td>".$person['country']."</td>";
					echo "<td>".$person['region']."</td>";
					echo "<td>".$person['numberrange']."</td>";
					echo "</tr>";
				}
			?>
        </tbody>
    </table>

	</div>
</section>

</body>
</html>