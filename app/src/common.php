<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    function openDatabase()
    {
        $dbEndpoint = getEnvVariableOrDefault("DB_ENDPOINT", "192.168.16.1");
        $dbName = getEnvVariableOrDefault("DB_NAME", "testdb");
        $dbUser = getEnvVariableOrDefault("DB_USER", "dbuser");
        $dbPassword = getEnvVariableOrDefault("DB_PASSWORD", "password");
        try
        {
            $conn = mysqli_connect($dbEndpoint, $dbUser, $dbPassword, $dbName);
            if ($conn) { return $conn; } 
        }
        catch (Exception $e)
        {
            echo "Database connection could not be established.<br/>";
            return null;
        }
    }

    function closeDatabase($conn)
    {
        mysqli_close($conn);
    }    

    function getEnvVariableOrDefault($envVariable, $default)
    {
        $value = getenv($envVariable);
        if (!$value) { $value = $default; }
        return $value;
    }

    $dbEndpoint = getEnvVariableOrDefault("DB_ENDPOINT", "192.168.16.1");
    $dbName = getEnvVariableOrDefault("DB_NAME", "testdb");
    $dbUser = getEnvVariableOrDefault("DB_USER", "dbuser");
    $dbPassword = getEnvVariableOrDefault("DB_PASSWORD", "password");
?>