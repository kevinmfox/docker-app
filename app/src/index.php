<?php
	session_start();
	include('common.php');

    $requestUri = $_SERVER['REQUEST_URI'];
    $jsonString = '{}';

    #we want a random user
    if ($requestUri == '/api/randomUser' && $_SERVER['REQUEST_METHOD'] == 'GET') {
        $conn = openDatabase();

        $sql = "SELECT
            name,
            phone,
            email,
            country,
            region,
            numberrange
        FROM people
        ORDER BY RAND() 
        LIMIT 1";
    
        $result = $conn->query($sql);
    
        $person = $result->fetch_assoc();
        $person['appHostname'] = gethostname();
        closeDatabase($conn);
        $jsonString = json_encode($person, JSON_PRETTY_PRINT);        
    }
    #we want all users
    elseif ($requestUri == '/api/allUsers'  && $_SERVER['REQUEST_METHOD'] == 'GET') {
        $conn = openDatabase();

        $sql = "SELECT
            name,
            phone,
            email,
            country,
            region,
            numberrange
        FROM people";
    
        $result = $conn->query($sql);
    
        $people = array();
        while ($row = $result->fetch_assoc()) {
            array_push($people, $row);
        }
        closeDatabase($conn);
        $people[0]['appHostname'] = gethostname();
        $jsonString = json_encode($people, JSON_PRETTY_PRINT);        
    }
    elseif ($requestUri == '/api/config' && $_SERVER['REQUEST_METHOD'] == 'GET') {
        $jsonString = '{"dbEndpoint": "'.$dbEndpoint.'", 
            "dbName": "'.$dbName.'",
            "dbUser": "'.$dbUser.'"
        }';
        $jsonString .= var_dump(getenv());
    }
    #uri isn't valid
    else {
        $jsonString = '{"errorMessage": "Invalid request"}';
    }
?>

<?php echo $jsonString; ?>
