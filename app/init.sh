#! /bin/bash
# make sure the user password hasn't already been supplied in plaintext
if [[ -z "${DB_PASSWORD}" ]]; then
  # check if a secrets file has been supplied
  if [[ ! -z "${DB_PASSWORD_FILE}" ]]; then
    # check if the secrets file exists
    if test -f "${DB_PASSWORD_FILE}"; then
      # set the user password to the secrets file contents
      export DB_PASSWORD=$(cat ${DB_PASSWORD_FILE})
    fi
  fi
fi

#run the php entrypoint
docker-php-entrypoint apache2-foreground