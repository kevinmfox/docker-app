- [Overview](#overview)
- [Database Tier](#database-tier)
- [App Tier](#app-tier)
- [Web Tier](#web-tier)
- [Swarm Deployment](#swarm-deployment)
- [I Want More Nodes](#i-want-more-nodes)
- [Docker Stack](#docker-stack)
- [Cleanup](#cleanup)

<a name="overview"></a>
# Overview
This was just me learning Docker. The goal was to build a three-tier app (sure, let's call it that ;), and have it run in a Docker swarm.

The applicaiton is a LAMP stack. Most of my docker commands were run on Windows 10 in PowerShell. If you're running on Mac or Linux, you might need to update line endings or path strings.

Test SQL data was grabbed from [here](https://generatedata.com/).

<a name="database-tier"></a>
# Database Tier
Assuming you've cloned this repo somewhere local, you should be able to run the below commands (with some noted modifications) to launch a single DB replica into a docker swarm.

**Note**: --constraint below has a hard-coded hostname. This is done to ensure the data mount works properly. Will need to be the hostname of whatever node the repo/data file are on (```docker node ls```).

**Note**: --publish doens't need to be included, but is helpful for troubleshooting.

**Note**: --mount. The 'source' below will need to be updated with the path to the mock SQL data.

```
docker swarm init
docker pull mysql:8
docker network create --subnet=172.20.0.0/16 -d overlay app-overlay
docker secret create db-root-password db/root-password.txt
docker secret create db-user-password db/user-password.txt
docker service create `
	--name db `
	--constraint node.hostname==docker-desktop `
	--env MYSQL_ROOT_PASSWORD_FILE=/run/secrets/db-root-password `
	--env MYSQL_USER=dbuser `
	--env MYSQL_PASSWORD_FILE=/run/secrets/db-user-password `
	--env MYSQL_DATABASE=testdb `
	--replicas 1 `
	--publish 3306:3306 `
    --network app-overlay `
 	--secret db-root-password `
	--secret db-user-password `
	--mount type=bind,source=[repo_location]/db/data,destination=/docker-entrypoint-initdb.d/ `
	mysql:8
```

Assuming you've published 3306 on your host, you shold be able to use the root or the 'dbuser' account to connect to the MySQL instance.

<a name="app-tier"></a>
# App Tier
You can pull the image from my Docker repo (```docker image pull kmfox/app```) or you can build it from source. If you do pull from my repo, it's convienent to *re-tag* it so most of the below works (```docker tag kmfox/app:latest app```).

Or, you can do it the fun way and build from source. Navigate into the /app folder.

```docker build -t app .```

If you want to test the app, you can try:

```docker run -dp 8080:80 -e DB_ENDPOINT=[host_ip] -e DB_PASSWORD=[password] app```

To get it running. Then you *should* be able to hit:

http://localhost:8080/api/randomUser

Which *should* come back with some JSON of a user.

**Note**: This assumes you published 3306 to your localhost when deploying the database. Also, you'll need to update the host_ip with your IP and the password with the user password passed to MySQL. Don't forget to stop this test deployment when you're done (```docker stop [container_id]```).

<a name="web-tier"></a>
# Web Tier
You can pull the image from my Docker repo (```docker image pull kmfox/web```) or you can build it from source. If you do pull from my repo, it's convienent to *re-tag* it so most of the below works (```docker tag kmfox/web:latest web```).

Or, build from source. Navigate into the /web folder.

```docker build -t web .```

The Web component won't work properly without the App tier, but you can still test it to ensure it's running:

```docker run -dp 80:80 web```

http://localhost should bring up the page.

Don't forget to stop the test container.

<a name="swarm-deployment"></a>
# Swarm Deployment
Assuming you've got your DB already running in the swarm as per the above, you *should* be able to deploy some App and Web nodes.

Let's get a few App containers running. The below will deploy 3 App tier containers into our previously created 'app-overlay' network. We can reference 'db' by the service name, and Docker will take care of finding the database for us. Also note the below will not allow direct access to the app layer (no published ports).

```
docker service create `
	--name app `
	--env DB_ENDPOINT=db `
	--env DB_PASSWORD_FILE=/var/run/secrets/db-user-password `
	--secret db-user-password `
	--replicas 3 `
	--publish 8080:80 `
	--network app-overlay `
	app
```

And then a few Web containers. The below deploys 3 Web tier containers into the 'app-overlay' network. We can reference the 'app' service to allow our Web tier to *find* our App tier. Port 80 on the localhost will be published.

```
docker service create `
    --name web `
    --env API_ENDPOINT=app `
    --replicas 3 `
    --network=app-overlay `
    --publish 80:80 `
    web
```

If you navigate to:

http://localhost/randomUser

You should see something similar to the following:

![](/assets/image1.jpg)

The "Web Hostname" and "App Hostname" will show the hostnames of the servers that were used. Refreshing typically gives a new App container, but I've found I needed to load a new browser session to get a new Web container (sticky sessions?).

<a name="i-want-more-nodes"></a>
# I Want More Nodes
I get it. DB, App, and Web all on the same node - what's the point?

Without going to far down the Docker rabbit hole, Docker makes it really simple to create more nodes.

Assuming you've still got your Swarm running (```docker swarm init```), you can run:

```docker swarm join-token manager``` or ```docker swarm join-token worker```

And it'll spit out commands that will allow you to add nodes to the Swarm. So, go ahead and spin-up a few AWS (or Azure if that's your thing) instances, and make sure they're on the same network. Once you run the ```docker service create``` commands, Docker will take care to distribute the work appropriately.

<a name="docker-stack"></a>
# Docker Stack
I played around a little bit with Docker Compose and stacks. The result is the ```docker-compose.yml``` file.

If you already have a Swarm running (```docker swarm init```), and you run:

```docker stack deploy -c .\docker-compose.yml test```

It should deploy the entire application. You can call it something other than 'test', just make sure to update the DB_ENDPOINT and API_ENDPOINT variables to match.

When I ran this on Ubuntu Server up in AWS, I did have to edit the paths within the compose file to be absolute. Not sure why, but didn't dig too far into it.

It also includes [dockersamples/visualizer](https://hub.docker.com/r/dockersamples/visualizer) deployed on port 8088. This gives a pretty cool view of where all the containers are running.

<a name="cleanup"></a>
# Cleanup 

Below are some helpful commands to help you cleanup either partially or completely.

```
docker service rm web
docker service rm app
docker service rm db

docker container prune --force

docker network rm app-overlay

docker secret rm db-root-password
docker secret rm db-user-password

docker swarm leave --force
```